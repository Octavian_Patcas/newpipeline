package com.controllers;

import com.dao.Company;
import com.domain.CompanyServiceImpl;
import com.dtos.CompanyDto;
import com.exceptions.ResourceNotFoundException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.service.CompanyService;
import org.hamcrest.Matchers;

import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.print.attribute.standard.Media;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.internal.bytebuddy.matcher.ElementMatchers.is;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CompanyControllerManagerImplTest {

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private MockMvc mockMvc;
 //   @MockBean
 //   CompanyRepository companyRepositoryMock;
    @MockBean
    CompanyServiceImpl companyServiceImpl;

    private List<CompanyDto> companyDtoList;
 //   @InjectMocks
 //   CompanyControllerImpl companyControllerImpl;

    @Before
    public void setUp() throws Exception{
   //     companyRepositoryMock = mock(CompanyRepository.class);
   //     companyServiceImpl = new CompanyServiceImpl(companyRepositoryMock);
   //     mockMvc = MockMvcBuilders.standaloneSetup(companyControllerImpl).build();
    }


    @Test
    void should_GetAllCompanies() throws Exception {
        this.companyDtoList = new ArrayList<>();
        this.companyDtoList.add(new CompanyDto( "user1@gmail.com", "pwd1"));
        this.companyDtoList.add(new CompanyDto( "user2@gmail.com", "pwd2"));
        this.companyDtoList.add(new CompanyDto( "user3@gmail.com", "pwd3"));
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/v1/companies").accept(MediaType.APPLICATION_JSON);
        given(companyServiceImpl.getAllCompanies()).willReturn(companyDtoList);
        this.mockMvc.perform(requestBuilder)
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.size()").value(companyDtoList.size()));

    }
    //Made with last tutorial - Unit testing = just for requests(Req mappings)
    @Test
    void should_GetCompany_ById() throws Exception {
       final Long companyDtoId = 1L;
       final CompanyDto companyDto = new CompanyDto("Zara","Croft");
       given(companyServiceImpl.getCompany(anyLong())).willReturn(Optional.of(companyDto));
       RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/v1/companies/{id}",companyDtoId).accept(MediaType.APPLICATION_JSON);
       this.mockMvc.perform(requestBuilder)
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.name").value(companyDto.getName()))
               .andExpect(jsonPath("$.location").value(companyDto.getLocation()));
    }

    @Test
    void should_Return404_When_GetCompany_ById() throws Exception {
        final Long id = 1L;;
        given(companyServiceImpl.getCompany(id)).willReturn(Optional.empty()); //Optional.empty();?

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/v1/companies/{id}",id).accept(MediaType.APPLICATION_JSON);
        this.mockMvc.perform(requestBuilder)
                    .andExpect(status().isNotFound());
    }

    @Test
    void should_addCompany() throws Exception {
        given(companyServiceImpl.addCompany(any(CompanyDto.class))).willAnswer(invocationOnMock -> invocationOnMock.getArgument(0));
        CompanyDto companyDto = new CompanyDto("Zara","Croft");
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/v1/companies").accept(MediaType.APPLICATION_JSON)
                                        .contentType(MediaType.APPLICATION_JSON)
                                        .content(objectMapper.writeValueAsString(companyDto));
        this.mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.name").value(companyDto.getName()))
                .andExpect(jsonPath("$.location").value(companyDto.getLocation()));
    }

    @Test
    void should_UpdateCompany_ById() throws Exception {
        Long id = 1L;
        CompanyDto companyDto = new CompanyDto( "Lara", "Croft");
        CompanyDto companyDto1 = new CompanyDto( "Alice", "WonderLand");
        Company company = new Company("Alice","WonderLand");
        // verify when with mapper!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        given(companyServiceImpl.getCompany(anyLong())).willReturn(Optional.of(companyDto));
        //IA ARGUMENTUL DE LA METODA CALLED
        given(companyServiceImpl.updateCompany(anyLong(),any(CompanyDto.class))).willReturn(company);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.put("/api/v1/companies/{id}",1L).accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(companyDto1));
        this.mockMvc.perform(requestBuilder)
                .andExpect(status().isCreated())
              //  .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.name").value("Alice"))
                .andExpect(jsonPath("$.location").value("WonderLand"));
    }

    @Test
    void should_Return404_When_Updating_NonExistingCompany() throws Exception {
        Long id = 1L;
        CompanyDto companyDto1 = new CompanyDto("A","B");
        given(companyServiceImpl.addCompany(any(CompanyDto.class))).willReturn(companyDto1);
        given(companyServiceImpl.getCompany(anyLong())).willReturn(Optional.empty()); //Optional.empty()?
        doThrow(ResourceNotFoundException.class).when(companyServiceImpl).updateCompany(anyLong(),any(CompanyDto.class));

        CompanyDto companyDto = new CompanyDto( "user", "Name");

        RequestBuilder requestBuilder = MockMvcRequestBuilders.put("/api/v1/companies/{id}",2L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(companyDto));

        this.mockMvc.perform(requestBuilder)
                .andExpect(status().isNotFound());

    }

    @Test
    void should_DeleteCompany_ById() throws Exception {
        Long id = 1L;
        CompanyDto companyDto = new CompanyDto( "Zara", "Croft");
        given(companyServiceImpl.getCompany(anyLong())).willReturn(Optional.of(companyDto));
        doNothing().when(companyServiceImpl).deleteCompany(id);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/api/v1/companies/{id}",id).accept(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(requestBuilder)
                .andExpect(status().isOk());
//                .andExpect(jsonPath("$.name").value(companyDto.getName()))
//                .andExpect(jsonPath("$.location").value(companyDto.getLocation()));
    }

    @Test
    void should_Return404_When_Deleting_NonExistingCompany() throws Exception {
        Long id = 10L;
        given(companyServiceImpl.getCompany(anyLong())).willReturn(Optional.empty()); //Optional.empty();?
        doThrow(ResourceNotFoundException.class).when(companyServiceImpl).deleteCompany((anyLong()));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/api/v1/companies/{id}",id).accept(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(requestBuilder)
                .andExpect(status().isNotFound());

    }

    @Test
    void should_DeleteAllCompanies() throws Exception {

        doNothing().when(companyServiceImpl).deleteAllCompanies();

        RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/api/v1/companies").accept(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(requestBuilder)
                .andExpect(status().isOk());
    }
}
