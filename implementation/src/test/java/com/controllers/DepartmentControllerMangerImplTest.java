package com.controllers;

import com.dao.Company;
import com.dao.Department;
import com.domain.DepartmentServiceImpl;
import com.dtos.CompanyDto;
import com.dtos.DepartmentDto;
import com.exceptions.ResourceNotFoundException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class DepartmentControllerMangerImplTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    DepartmentServiceImpl departmentService;

    private List<DepartmentDto> departmentDtoList;



    @Test
    void should_GetAllDepartments() throws Exception {
        CompanyDto companyDto = new CompanyDto("Ubisoft","France");
        this.departmentDtoList = new ArrayList<>();
        this.departmentDtoList.add(new DepartmentDto( "user1@gmail.com", 2L,companyDto));
        this.departmentDtoList.add(new DepartmentDto( "user2@gmail.com", 3L,companyDto));
        this.departmentDtoList.add(new DepartmentDto( "user3@gmail.com", 4L,companyDto));
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/v1/companies/{id}/departments",1L).accept(MediaType.APPLICATION_JSON);
        given(departmentService.getAllDepartments(anyLong())).willReturn(departmentDtoList);
        this.mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(departmentDtoList.size()));
    }

    @Test
    void should_GetDepartment_ById() throws Exception {
        CompanyDto companyDto = new CompanyDto("Ubisoft","France");
        final Long id = 1L;
        final DepartmentDto departmentDto = new DepartmentDto("Zara",2L,companyDto);
        given(departmentService.getDepartment(anyLong(),anyLong())).willReturn(Optional.of(departmentDto));
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/v1/companies/{companyId}/departments/{id}",1L,1L).accept(MediaType.APPLICATION_JSON);
        this.mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(departmentDto.getName()))
                .andExpect(jsonPath("$.floor").value(departmentDto.getFloor()));
    }

    @Test
    void should_Return404_When_GetDepartment_ById() throws Exception {
        final Long id = 1L;
        given(departmentService.getDepartment(anyLong(),anyLong())).willReturn(Optional.empty()); //Optional.empty();?

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/v1/companies/{companyId}/departments/{id}",id,id).accept(MediaType.APPLICATION_JSON);
        this.mockMvc.perform(requestBuilder)
                .andExpect(status().isNotFound());
    }
    @Test
    void should_AddDepartment() throws Exception {
        CompanyDto companyDto = new CompanyDto("Ubisoft","France");
        given(departmentService.addDepartment(anyLong(),any(DepartmentDto.class))).willAnswer(invocationOnMock -> invocationOnMock.getArgument(1));
        DepartmentDto departmentDto = new DepartmentDto("ANA",2L,companyDto);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/v1/companies/{companyId}/departments",1L).accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(departmentDto));
        this.mockMvc.perform(requestBuilder)
                .andExpect(jsonPath("$.name").value(departmentDto.getName()))
                .andExpect(jsonPath("$.floor").value(departmentDto.getFloor()));
    }

    @Test
    void should_UpdateDepartment_ById() throws Exception {
        Long id = 1L;
        CompanyDto companyDto = new CompanyDto( "Lara", "Croft");
        Company company = new Company( "Lara", "Croft");
        DepartmentDto departmentDto = new DepartmentDto("Ana",2L,companyDto);
        DepartmentDto departmentDto1 = new DepartmentDto("NEW ANA",3L,companyDto);

        given(departmentService.getDepartment(anyLong(),anyLong())).willReturn(Optional.of(departmentDto));
        Department department = new Department("NEW ANA",3L,company);
        //IA ARGUMENTUL DE LA METODA CALLED
        given(departmentService.updateDepartment(any(DepartmentDto.class),Mockito.anyLong())).willReturn(department);
//        given(companyServiceImpl.updateCompany(Mockito.anyLong(),any(CompanyDto.class))).willReturn(companyDto1);
        RequestBuilder requestBuilder = MockMvcRequestBuilders.put("/api/v1/companies/{companyId}/departments/{id}",id,id).accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(departmentDto1));
        this.mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.name").value("NEW ANA"))
                .andExpect(jsonPath("$.floor").value(3L));
    }

    @Test
    void should_Return404_When_Updating_NonExistingDepartment() throws Exception {
        CompanyDto companyDto1 = new CompanyDto("A","B");
        DepartmentDto departmentDto = new DepartmentDto("DEP",1L,companyDto1);
        given(departmentService.addDepartment(anyLong(),any(DepartmentDto.class))).willReturn(departmentDto);
        given(departmentService.getDepartment(anyLong(),anyLong())).willReturn(Optional.empty()); //Optional.empty()?
        doThrow(ResourceNotFoundException.class).when(departmentService).updateDepartment(any(DepartmentDto.class),anyLong());

        CompanyDto companyDto = new CompanyDto("COM","PANY");
        DepartmentDto departmentDto1 = new DepartmentDto("NEW",2L,companyDto);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.put("/api/v1/companies/{companyId}/departments/{id}",2L,3L)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(departmentDto1));

        this.mockMvc.perform(requestBuilder)
                .andExpect(status().isNotFound());

    }
    @Test
    void should_DeleteDepartment_ById() throws Exception {
        Long id = 1L;
        CompanyDto companyDto = new CompanyDto( "Zara", "Croft");
        DepartmentDto departmentDto = new DepartmentDto("DEP",3L,companyDto);
        given(departmentService.getDepartment(anyLong(),anyLong())).willReturn(Optional.of(departmentDto));
        doNothing().when(departmentService).deleteDepartment(anyLong());

        RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/api/v1/companies/{companyId}/departments/{id}",id,id).accept(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(requestBuilder)
                .andExpect(status().isOk());
    }

    @Test
    void should_Return404_When_Deleting_NonExistingDepartment() throws Exception {
        Long id = 1L;
        CompanyDto companyDto = new CompanyDto("A","B");
        given(departmentService.getDepartment(anyLong(),anyLong())).willReturn(Optional.empty()); //Optional.empty();?
        doThrow(ResourceNotFoundException.class).when(departmentService).deleteDepartment(anyLong());

        RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/api/v1/companies/{companyId}/departments/{id}",25L,10L).accept(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(requestBuilder)
                .andExpect(status().isNotFound());

    }

 /*   @Test
    void should_DeleteAllDepartments() throws Exception {
        Long id =1L;
        doNothing().when(departmentService).deleteAllDepartments();

        RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/api/v1/companies/{companyId}/departments",id).accept(MediaType.APPLICATION_JSON);

        this.mockMvc.perform(requestBuilder)
                .andExpect(status().isOk());
    }*/
}
