package com.domain;

import com.dao.Company;
import com.dao.CompanyRepository;
import com.dao.Department;
import com.dao.DepartmentRepository;
import com.domain.mapper.department.mapperinterface.DepartmentMapper;
import com.dtos.CompanyDto;
import com.dtos.DepartmentDto;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DepartmentServiceImplTest {

    @Mock
    private DepartmentRepository departmentRepository;

    @Mock
    private CompanyRepository companyRepository;

    @InjectMocks
    private DepartmentServiceImpl departmentService;


    //null?
  /*  @Test
    void should_GetAllDepartments() {
        CompanyDto companyDto = new CompanyDto("THE","COMPANY");
        Company company = new Company("THE","COMPANY");
        List<Department> departmentList = new ArrayList();
        departmentList.add(new Department("Comp1",1L,company));
        departmentList.add(new Department( "Comp2",2L,company));
        departmentList.add(new Department("Comp3",3L,company));

        given(departmentRepository.findByCompanyId(anyLong())).willReturn(departmentList);

        List<DepartmentDto> streamDepartments = departmentList.stream().map(department -> new DepartmentDto(department.getName(), department.getFloor(),companyDto))
                .collect(Collectors.toList());

        List<DepartmentDto> expected = departmentService.getAllDepartments(1L);

       // Assert.assertTrue(new ReflectionEquals(expected).matches(streamDepartments));

        Assert.assertEquals(expected.size(),streamDepartments.size());
        Assert.assertEquals(expected.get(0).getName(),streamDepartments.get(0).getName());
        Assert.assertEquals(expected.get(1).getName(),streamDepartments.get(1).getName());
        Assert.assertEquals(expected.get(2).getName(),streamDepartments.get(2).getName());
    }*/

    //null?
    @Test
    void should_GetDepartment_ById() {
        final Long id = 1L;
        Company company = new Company("ALIE","ANA");
        final Department department = new Department( "Ubisoft",1L,company);

        lenient().when(departmentRepository.findById(id)).thenReturn(Optional.of(department));

        final Optional<DepartmentDto> expected  = departmentService.getDepartment(1L,1L);

       // Assert.assertNotNull(expected);
        Assert.assertEquals(expected.get().getName(),department.getName());
        Assert.assertEquals(expected.get().getFloor(),department.getFloor());
    }

    //Doesnt work because?? // null exception
    @Test
    void should_AddDepartment() {
        final Company company = new Company( "Ubisoft","France");
        final Department department = new Department("DEP",2L,company);

        final CompanyDto companyDto = new CompanyDto( "Ubisoft","France");
        final DepartmentDto departmentDto = new DepartmentDto("DEP",2L,companyDto);

//        when(companyRepository.findById(company.getId())).thenReturn(Optional.empty());
        lenient().when(departmentRepository.save(department)).thenAnswer(invocation -> invocation.getArgument(0));
        when(companyRepository.findById(anyLong())).thenReturn(Optional.of(company));

        DepartmentDto departmentDto1 = departmentService.addDepartment(1L,departmentDto);

        Assert.assertNotNull(departmentDto1);
        Assert.assertEquals(departmentDto1.getName(),department.getName());
        Assert.assertEquals(departmentDto1.getFloor(),department.getFloor());

        verify(departmentRepository).save(any(Department.class));
    }

    //NULL
    @Test
    void should_UpdateDepartment_ById() {
        final Company company = new Company( "Ubisoft","France");;
        final Department department = new Department("DEP",2L,company);

        final CompanyDto company1 = new CompanyDto( "Ubisoft","Ubisoft");;
        final DepartmentDto departmentDto = new DepartmentDto("DEPUPDATE",5L,company1);

        lenient().when(departmentRepository.save(department)).thenReturn(department);
        when(departmentRepository.findById(any())).thenReturn(Optional.of(new Department()));


         Department expected = departmentService.updateDepartment(departmentDto,1L);
        Optional<Department> actual = departmentRepository.findById(2L);

       // Assert.assertNotNull(expected);
        Assert.assertEquals(expected.getName(),actual.get().getName());
        Assert.assertEquals(expected.getFloor(),actual.get().getFloor());

        verify(departmentRepository).save(any(Department.class));
    }

    //NULL
    @Test
    void should_DeleteDepartment_ById() {
        final Long id=1L;

        departmentService.deleteDepartment(id);
        departmentService.deleteDepartment(id);


        verify(departmentRepository, times(2)).deleteById(id);
    }

    @Test
    void should_DeleteAllDepartments(){

        departmentService.deleteAllDepartments();

        verify(departmentRepository,times(1)).deleteAll();
    }
}
