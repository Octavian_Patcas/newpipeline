package com.domain;

import com.dao.Company;
import com.dao.CompanyRepository;
import com.dao.DepartmentRepository;
import com.domain.mapper.company.mapperinterface.CompanyMapper;
import com.dtos.CompanyDto;
import com.exceptions.ResourceNotFoundException;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CompanyServiceImplTest {

    @Mock
    private CompanyRepository companyRepository;

    @Mock
    private CompanyMapper companyMapper;

    @Mock
    private DepartmentServiceImpl departmentService;

    @InjectMocks
    private CompanyServiceImpl companyService;

    @Test
    void should_GetAllCompanies() {
        List<Company> companies = new ArrayList();
        companies.add(new Company("Comp1","Zoom"));
        companies.add(new Company( "Comp2","Vroom"));
        companies.add(new Company( "Comp3","Boom"));
        List<CompanyDto> companiesDto = new ArrayList();
        CompanyDto comp1;
        CompanyDto comp2;
        CompanyDto comp3;
        companiesDto.add(comp1 = new CompanyDto("Comp1","Zoom"));
        companiesDto.add(comp2 = new CompanyDto( "Comp2","Vroom"));
        companiesDto.add(comp3 = new CompanyDto( "Comp3","Boom"));

        when(companyRepository.findAll()).thenReturn(companies);
        when(companyMapper.toCompanyDtoList(any())).thenReturn(companiesDto);

/*        List<CompanyDto> streamCompanies = companies.stream().map(company -> new CompanyDto(company.getName(), company.getLocation()))
                .collect(Collectors.toList());*/

        //What we get from service method
        List<CompanyDto> expected = companyService.getAllCompanies();

      //  Assert.assertTrue(new ReflectionEquals(expected).matches(streamCompanies));
        Assert.assertEquals(expected.size(),companies.size());
        Assert.assertTrue(expected.contains(comp1));
        Assert.assertTrue(expected.contains(comp2));
        Assert.assertTrue(expected.contains(comp3));
        //not the same instance so it fails
       // Assert.assertEquals(expected, streamCompanies);
    }

    @Test
    void should_GetCompany_ById() {
        final Company company = new Company( "Ubisoft","France");

        given(companyRepository.findById(anyLong())).willReturn(Optional.of(company));

        final Optional<CompanyDto> expected = companyService.getCompany(anyLong());

        //Assert.assertNotNull(expected);
        Assert.assertEquals(expected.get().getName(),company.getName());
        Assert.assertEquals(expected.get().getLocation(),company.getLocation());
    }
    @Test
    void should_ThrowResourceNotFoundException_When_GetCompany_ById() {
        final Long id = 1L;

        doThrow(ResourceNotFoundException.class).when(companyRepository).findById(id);

        Assert.assertThrows(ResourceNotFoundException.class,()->{
            companyRepository.findById(id);
        });

    }

    //save has stub with diff args in serviceimpl
    @Test
    void should_AddCompany() {
        final Company company = new Company( "Ubisoft","France");
        final CompanyDto companyDto = new CompanyDto("Ubisoft","France");

//        when(companyRepository.findById(company.getId())).thenReturn(Optional.empty());
        when(companyRepository.save(company)).thenAnswer(invocation -> invocation.getArgument(0));
        when(companyMapper.toCompanyDao(any(CompanyDto.class))).thenReturn(company);

        CompanyDto savedCompany = companyService.addCompany(companyDto);

        //Assert.assertNotNull(expected);
        Assert.assertEquals(savedCompany.getName(),company.getName());
        Assert.assertEquals(savedCompany.getLocation(),company.getLocation());

        verify(companyRepository).save(any(Company.class));
    }

    @Test
    void should_UpdateCompany() {
        final Company company = new Company( "Ubisoft","France");
        final CompanyDto companyDto = new CompanyDto( "UPDATE","UPDATE");

        lenient().when(companyRepository.save(company)).thenReturn(company);
        //added this cause it also does findbyid in service method
        when(companyRepository.findById(anyLong())).thenReturn(Optional.of(new Company()));

        Company expected = companyService.updateCompany(1L,companyDto);
        Optional<Company> company1 = companyRepository.findById(1L);

        Assert.assertNotNull(company1.get());
        Assert.assertEquals(expected.getName(),company1.get().getName());
        Assert.assertEquals(expected.getLocation(),company1.get().getLocation());

        verify(companyRepository).save(any(Company.class));
    }
    @Test
    void should_ThrowResourceNotFoundException_When_UpdateCompany_ById() {
        final Long id = 1L;
        final Company company = new Company( "Ubisoft","France");

        lenient().when(companyRepository.findById(id)).thenReturn(Optional.empty());
        doThrow(ResourceNotFoundException.class).when(companyRepository).save(company);

        Assert.assertThrows(ResourceNotFoundException.class,()->{
            companyRepository.save(company);
        });
    }

    @Test
    void should_DeleteCompany_ById() {
        final Long id=1L;

        companyService.deleteCompany(id);
        companyService.deleteCompany(id);

        verify(companyRepository, times(2)).deleteById(id);
    }

    @Test
    void should_ThrowResourceNotFoundException_When_DeleteBy_InvalidId() {
        final Long id=1L;

        doThrow(ResourceNotFoundException.class).when(companyRepository).deleteById(id);

        Assert.assertThrows(ResourceNotFoundException.class,()->{
            companyRepository.deleteById(id);
        });
     //   verify(companyRepository, never()).deleteById(id);
    }

    @Test
    void should_DeleteAllCompanies(){

        companyService.deleteAllCompanies();

        verify(departmentService,times(1)).deleteAllDepartments();
        verify(companyRepository,times(1)).deleteAll();
    }
}
