package com.dao;

import com.dtos.DepartmentDto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface DepartmentRepository  extends CrudRepository<Department,Long> {
    List<Department> findByCompanyId(Long CompanyId);
}
