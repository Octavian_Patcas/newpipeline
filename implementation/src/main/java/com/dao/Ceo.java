package com.dao;

import javax.persistence.*;

@Entity
@Table(name = "CeoTable")
public class Ceo {
    @Id
    @GeneratedValue
    @Column(name = "ceo_id")
    private Long id;
    private String name;
    private Long age;
    @OneToOne
    private Company company;

    public Ceo(){}
    public Ceo(String name, Long age, Company company) {
        this.name = name;
        this.age = age;
        this.company = company;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
