package com.dao;

import javax.persistence.*;

@Entity
@Table(name = "company")
public class Company {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String location;

    @OneToOne(cascade = CascadeType.ALL,mappedBy = "company")
    private Ceo ceo;

    public Company(){

    }
    public Company (String name, String location) {
        super();

        this.name = name;
        this.location = location;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
