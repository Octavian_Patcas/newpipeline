package com.dao;

import org.springframework.data.repository.CrudRepository;


public interface CeoRepository extends CrudRepository<Ceo,Long> {
    Ceo findCeoByCompanyId(Long id);
}
