package com.dao;

import javax.persistence.*;

@Entity
@Table(name = "department")
public class Department {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private Long floor;
    @ManyToOne(cascade = CascadeType.ALL)
    private Company company;

    public Department(){

    }
    public Department(String name, Long floor,Company company) {
        super();

        this.name = name;
        this.floor = floor;
        this.company= company;
    }

    public Long getFloor() {
        return floor;
    }

    public void setFloor(Long floor) {
        this.floor = floor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
