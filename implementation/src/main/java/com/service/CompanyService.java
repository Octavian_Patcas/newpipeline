package com.service;

import com.dao.Company;
import com.dtos.CompanyDto;
import java.util.List;
import java.util.Optional;

public interface CompanyService {

    List<CompanyDto> getAllCompanies();

    Optional<CompanyDto> getCompany(Long id);

    CompanyDto addCompany(CompanyDto companyDto);

    Company updateCompany(Long id, CompanyDto companyDto);

    void deleteCompany(Long id);

    void deleteAllCompanies();
}
