package com.service;

import com.dao.Ceo;
import com.dtos.CeoDto;

import java.util.Optional;

public interface CeoService {

    CeoDto getCeo(Long companyId);
    CeoDto addCeo(Long id,CeoDto ceoDto);
    Ceo updateCeo(CeoDto ceoDto);
    void deleteCeo();

}
