package com.service;

import com.dao.Department;
import com.dtos.DepartmentDto;

import java.util.List;
import java.util.Optional;

public interface DepartmentService {
    List<DepartmentDto> getAllDepartments(Long companyId);

    Optional<DepartmentDto> getDepartment(Long id, Long companyId);

    DepartmentDto addDepartment(Long companyId, DepartmentDto departmentDto);

    Department updateDepartment(DepartmentDto departmentDto, Long id);

    void deleteDepartment(Long id);

    void deleteAllDepartments();
}
