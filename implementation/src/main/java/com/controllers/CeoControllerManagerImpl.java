package com.controllers;

import com.dtos.CeoDto;
import com.managers.CeoControllerMangaer;
import com.service.CeoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CeoControllerManagerImpl implements CeoControllerMangaer {

    private CeoService ceoService;

    @Autowired
    public CeoControllerManagerImpl(CeoService ceoService) {
        this.ceoService = ceoService;
    }

    @Override
    public CeoDto getCeo(Long id) {
        return ceoService.getCeo(id);
    }

    @Override
    public CeoDto addCeo(Long id,CeoDto ceoDto) {
        return ceoService.addCeo(id,ceoDto);
    }

}
