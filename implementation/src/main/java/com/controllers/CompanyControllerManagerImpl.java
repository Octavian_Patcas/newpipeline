package com.controllers;

import com.domain.mapper.company.mapperinterface.CompanyMapper;
import com.managers.CompanyControllerManager;
import com.service.CompanyService;
import com.dtos.CompanyDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CompanyControllerManagerImpl implements CompanyControllerManager {

    private CompanyService companyService;
    private CompanyMapper companyMapper;

    @Autowired
    public CompanyControllerManagerImpl(CompanyService companyService, CompanyMapper companyMapper) {
        this.companyService = companyService;
        this.companyMapper = companyMapper;
    }

    @Override
    public List<CompanyDto> getAllCompanies() {
        return companyService.getAllCompanies();
    }

    //changed to responseentity
    @Override
    public ResponseEntity<CompanyDto> getCompany(Long id) {
        return companyService.getCompany(id).map(
                ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @Override
    public CompanyDto addCompany(CompanyDto companyDto) {
        return companyService.addCompany(companyDto);
    }

    @Override
    public ResponseEntity<CompanyDto> updateCompany(Long id,CompanyDto companyDto) {
         return ResponseEntity.status(HttpStatus.CREATED)
                 .body(companyMapper.toCompanyDto(companyService.updateCompany(id, companyDto)));
    }

    @Override
    public ResponseEntity deleteCompany(Long id) {
        companyService.deleteCompany(id);
        return ResponseEntity.status(HttpStatus.OK).build();

    }

    @Override
    public ResponseEntity deleteAllCompanies() {
        companyService.deleteAllCompanies();
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
