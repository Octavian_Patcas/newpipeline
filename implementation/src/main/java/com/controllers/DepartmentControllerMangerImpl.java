package com.controllers;

import com.domain.mapper.department.mapperinterface.DepartmentMapper;
import com.managers.DepartmentControllerManger;
import com.service.DepartmentService;
import com.dtos.DepartmentDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@RestController
public class DepartmentControllerMangerImpl implements DepartmentControllerManger {

    private DepartmentService departmentService;

    private DepartmentMapper departmentMapper;

    @Autowired
    public DepartmentControllerMangerImpl(DepartmentService departmentService, DepartmentMapper departmentMapper) {
        this.departmentService = departmentService;
        this.departmentMapper = departmentMapper;
    }

    @Override
    public List<DepartmentDto> getAllDepartments(Long id) {
        return departmentService.getAllDepartments(id);
    }

    @Override
    public ResponseEntity<DepartmentDto> getDepartment(Long id, Long companyId) {
        return departmentService.getDepartment(id,companyId).map(
                ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @Override
    public DepartmentDto addDepartment(Long id,DepartmentDto departmentDto) {
    //    departmentDto.setCompanyDto(new CompanyDto(id,"",""));
       return departmentService.addDepartment(id,departmentDto);
    }

    @Override
    public DepartmentDto updateDepartment(DepartmentDto departmentDto, Long id) {
    //    departmentDto.setCompanyDto(new CompanyDto(companyId,"",""));
       return departmentMapper.toDepartmentDto(departmentService.updateDepartment(departmentDto,id));
    }

    @Override
    public ResponseEntity deleteDepartment(Long id) {
        departmentService.deleteDepartment(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

  /*  @Override
    public ResponseEntity deleteAllDepartments() {
        departmentService.deleteAllDepartments();
        return ResponseEntity.status(HttpStatus.OK).build();

    }*/
}
