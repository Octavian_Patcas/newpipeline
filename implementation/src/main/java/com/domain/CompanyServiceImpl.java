package com.domain;

import com.domain.mapper.company.mapperinterface.CompanyMapper;
import com.service.CompanyService;
import com.dao.Company;
import com.dtos.CompanyDto;
import com.exceptions.ResourceNotFoundException;
import com.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.dao.CompanyRepository;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CompanyServiceImpl implements CompanyService {

    private CompanyMapper companyMapper;

    private CompanyRepository companyRepository;

    private DepartmentService departmentService;

    @Autowired
    public CompanyServiceImpl(CompanyMapper companyMapper, CompanyRepository companyRepository, DepartmentService departmentService) {
        this.companyMapper = companyMapper;
        this.companyRepository = companyRepository;
        this.departmentService = departmentService;
    }

    public List<CompanyDto> getAllCompanies(){
        /*List<CompanyDto> companies = new ArrayList<>();
        companyRepository.findAll().forEach(company -> companies.add(companyMapper.toCompanyDto(company));
        return companies;*/
      List<Company> companies = new ArrayList<>();
        companyRepository.findAll()
                .forEach(company -> companies.add(company));
        return companyMapper.toCompanyDtoList(companies);
    }
//changed to optional
    public Optional<CompanyDto> getCompany(Long id){
        try{
            CompanyDto companyDto = new CompanyDto();
            companyRepository.findById(id).map(company -> {
                companyDto.setName(company.getName());
                companyDto.setLocation(company.getLocation());
                return companyDto;
            });
            return Optional.of(companyDto);
            }
        catch (Exception e){
            throw new ResourceNotFoundException(e.getMessage());
        }
    }

    public CompanyDto addCompany(CompanyDto companyDto){
        Company company = companyMapper.toCompanyDao(companyDto);
/*        company.setName(companyDto.getName());
        company.setLocation(companyDto.getLocation());*/
        companyRepository.save(company);
        return companyDto;
    }

    public Company updateCompany(Long id, CompanyDto companyDto){
        try{
            Company company = companyRepository.findById(id).get(); //returned optional
            company.setName(companyDto.getName());
            company.setLocation(companyDto.getLocation());
            companyRepository.save(company);
            return company;
            }
        catch (Exception e){
            throw new ResourceNotFoundException(e.getMessage());
        }

    }

    public void deleteCompany(Long id){
        try{companyRepository.deleteById(id);}
        catch (Exception e){
            throw new ResourceNotFoundException(e.getMessage());
        }

    }

    @Override
    public void deleteAllCompanies() {
        departmentService.deleteAllDepartments();
        companyRepository.deleteAll();
    }
}
