package com.domain;

import com.dao.CompanyRepository;
import com.domain.mapper.department.mapperinterface.DepartmentMapper;
import com.service.DepartmentService;
import com.dao.Company;
import com.dao.Department;
import com.dtos.CompanyDto;
import com.dtos.DepartmentDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.dao.DepartmentRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DepartmentServiceImpl implements DepartmentService {

    private DepartmentMapper departmentMapper;

    private DepartmentRepository departmentRepository;

    private CompanyRepository companyRepository;

    @Autowired
    public DepartmentServiceImpl(DepartmentMapper departmentMapper, DepartmentRepository departmentRepository, CompanyRepository companyRepository) {
        this.departmentMapper = departmentMapper;
        this.departmentRepository = departmentRepository;
        this.companyRepository = companyRepository;
    }

    public List<DepartmentDto> getAllDepartments(Long companyId){
        List<DepartmentDto> departments = new ArrayList<>();
        departmentRepository.findByCompanyId(companyId)
                .forEach(department -> {
                    /*CompanyDto companyDto = new CompanyDto();
                    Company company = department.getCompany();
                 //   companyDto.setId(companyId);
                    companyDto.setName(company.getName());
                    companyDto.setLocation(company.getLocation());*/
                    departments.add(departmentMapper.toDepartmentDto(department));});
        return departments;
    }

    public Optional<DepartmentDto> getDepartment(Long id, Long companyId){
        DepartmentDto departmentDto = new DepartmentDto();
        CompanyDto companyDto = new CompanyDto();
        departmentRepository.findById(id).map(department -> {
            Company company = department.getCompany();
        //    companyDto.setId(companyId);
            companyDto.setName(company.getName());
            companyDto.setLocation(company.getLocation());
            departmentDto.setCompanyDto(companyDto);
         //   departmentDto.setId(department.getId());
            departmentDto.setFloor(department.getFloor());
            departmentDto.setName(department.getName());
            return departmentDto;
        });
        return Optional.of(departmentDto);
    }

    @Override
    public DepartmentDto addDepartment(Long companyId, DepartmentDto departmentDto) {
        Department department = new Department();
//        company.setLocation(departmentDto.getCompanyDto().getLocation());
//        company.setName(departmentDto.getCompanyDto().getName());
   //     company.setId(companyId);
        department.setCompany(companyRepository.findById(companyId).get());
        //department.setId(departmentDto.getId());
        department.setFloor(departmentDto.getFloor());
        department.setName(departmentDto.getName());
        departmentRepository.save(department);
        return departmentDto;
    }

    @Override
    public Department updateDepartment(DepartmentDto departmentDto, Long id) {
        Department department = departmentRepository.findById(id).get();
      //  department.setId(id);
        department.setName(departmentDto.getName());
        department.setFloor(departmentDto.getFloor());
        departmentRepository.save(department);
        return  department;
    }

    @Override
    public void deleteDepartment(Long id) {
        departmentRepository.deleteById(id);
    }

    @Override
    public void deleteAllDepartments() {
        departmentRepository.deleteAll();
    }


}
