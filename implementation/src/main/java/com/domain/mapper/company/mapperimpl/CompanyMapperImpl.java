package com.domain.mapper.company.mapperimpl;

import com.dao.Company;
import com.domain.mapper.company.mapperinterface.CompanyMapper;
import com.dtos.CompanyDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Component
public class CompanyMapperImpl implements CompanyMapper {
    @Override
    public CompanyDto toCompanyDto(Company company) {
        return new CompanyDto(company.getName(),company.getLocation());
    }

    @Override
    public Company toCompanyDao(CompanyDto companyDto) {
        return new Company(companyDto.getName(),companyDto.getLocation());
    }

    @Override
    public List<CompanyDto> toCompanyDtoList(List<Company> companies) {
        return companies.stream().map(company -> new CompanyDto(company.getName(),company.getLocation()))
                .collect(Collectors.toList());
    }
}
