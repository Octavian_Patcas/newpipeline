package com.domain.mapper.company.mapperinterface;

import com.dao.Company;
import com.dtos.CompanyDto;

import java.util.List;

public interface CompanyMapper {

    CompanyDto toCompanyDto(Company company);

    Company toCompanyDao(CompanyDto companyDto);

    List<CompanyDto> toCompanyDtoList(List<Company> companies);
}
