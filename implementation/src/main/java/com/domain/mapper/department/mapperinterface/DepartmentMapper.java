package com.domain.mapper.department.mapperinterface;

import com.dao.Department;
import com.dtos.DepartmentDto;

import java.util.List;

public interface DepartmentMapper {
    DepartmentDto toDepartmentDto(Department department);
    Department toDepartmentDao(DepartmentDto departmentDto);
    List<DepartmentDto> toDepartmentDtoList(List<Department> departmentList);
}
