package com.domain.mapper.department.mapperimpl;

import com.dao.Department;
import com.domain.mapper.company.mapperinterface.CompanyMapper;
import com.domain.mapper.department.mapperinterface.DepartmentMapper;
import com.dtos.DepartmentDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class DepartmentMapperImpl implements DepartmentMapper {

    private CompanyMapper companyMapper;

    @Autowired
    public DepartmentMapperImpl(CompanyMapper companyMapper) {
        this.companyMapper = companyMapper;
    }

    @Override
    public DepartmentDto toDepartmentDto(Department department) {
        return new DepartmentDto(department.getName(),department.getFloor(),companyMapper.toCompanyDto(department.getCompany()));
    }

    @Override
    public Department toDepartmentDao(DepartmentDto departmentDto) {
        return new Department(departmentDto.getName(),departmentDto.getFloor(),companyMapper.toCompanyDao(departmentDto.getCompanyDto()));
    }

    @Override
    public List<DepartmentDto> toDepartmentDtoList(List<Department> departmentList) {
        return departmentList.stream().map(department -> new DepartmentDto(department.getName(),department.getFloor(),companyMapper.toCompanyDto(department.getCompany())))
                .collect(Collectors.toList());
    }
}
