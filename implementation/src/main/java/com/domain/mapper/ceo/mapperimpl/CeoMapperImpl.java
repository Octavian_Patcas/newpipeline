package com.domain.mapper.ceo.mapperimpl;

import com.dao.Ceo;
import com.domain.mapper.ceo.mapperinterface.CeoMapper;
import com.domain.mapper.company.mapperinterface.CompanyMapper;
import com.dtos.CeoDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CeoMapperImpl implements CeoMapper {

    private CompanyMapper companyMapper;

    @Autowired
    public CeoMapperImpl(CompanyMapper companyMapper) {
        this.companyMapper = companyMapper;
    }

    @Override
    public CeoDto toCeoDto(Ceo ceo) {
        return new CeoDto(ceo.getName(),ceo.getAge(),companyMapper.toCompanyDto(ceo.getCompany()));
    }

    @Override
    public Ceo toCeoDao(CeoDto ceoDto) {
        return new Ceo(ceoDto.getName(),ceoDto.getAge(),companyMapper.toCompanyDao(ceoDto.getCompanyDto()));
    }
}
