package com.domain.mapper.ceo.mapperinterface;

import com.dao.Ceo;
import com.dtos.CeoDto;

public interface CeoMapper {

    CeoDto toCeoDto (Ceo ceo);
    Ceo toCeoDao(CeoDto ceoDto);
}
