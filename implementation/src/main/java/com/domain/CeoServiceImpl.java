package com.domain;

import com.dao.Ceo;
import com.dao.CeoRepository;
import com.dao.Company;
import com.dao.CompanyRepository;
import com.domain.mapper.ceo.mapperinterface.CeoMapper;
import com.domain.mapper.company.mapperinterface.CompanyMapper;
import com.dtos.CeoDto;
import com.dtos.CompanyDto;
import com.service.CeoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CeoServiceImpl implements CeoService {

    private CeoMapper ceoMapper;

    private CompanyMapper companyMapper;

    private CeoRepository ceoRepository;

    private CompanyRepository companyRepository;

    @Autowired
    public CeoServiceImpl(CompanyMapper companyMapper, CeoRepository ceoRepository, CompanyRepository companyRepository) {
        this.companyMapper = companyMapper;
        this.ceoRepository = ceoRepository;
        this.companyRepository = companyRepository;
    }

    @Override
    public CeoDto getCeo(Long companyId) {
        CeoDto ceoDto = new CeoDto();
        Company company;
        company = companyRepository.findById(companyId).get();
        CompanyDto companyDto = companyMapper.toCompanyDto(company);
        System.out.println(ceoRepository.findCeoByCompanyId(companyId));
        ceoDto.setName("REX");
        ceoDto.setAge(25L);
        ceoDto.setCompanyDto(companyDto);
        return ceoDto;
    }

    @Override
    public CeoDto addCeo(Long id,CeoDto ceoDto) {
        Ceo ceo = new Ceo();
        ceo.setName(ceoDto.getName());
        ceo.setAge(ceoDto.getAge());
        ceo.setCompany(companyRepository.findById(id).get());
        System.out.println(companyRepository.findById(id).get().getName());
        ceoRepository.save(ceo);
        return ceoDto;
    }

    @Override
    public Ceo updateCeo(CeoDto ceoDto) {
        return null;
    }

    @Override
    public void deleteCeo() {

    }
}
