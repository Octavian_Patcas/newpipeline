package com.managers;

import com.dtos.CeoDto;
import com.dtos.CompanyDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("api/v1/companies")
public interface CeoControllerMangaer {

    @GetMapping("/{id}/ceo")
    @ResponseStatus(HttpStatus.OK)
    CeoDto getCeo(@PathVariable Long id);

    @PostMapping("/{id}/ceo")
    @ResponseStatus(HttpStatus.CREATED)
    CeoDto addCeo(@PathVariable Long id,@RequestBody CeoDto ceoDto);
}
