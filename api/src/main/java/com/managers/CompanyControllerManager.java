package com.managers;

import com.dtos.CompanyDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.util.List;
@RequestMapping("api/v1")
public interface CompanyControllerManager {

    @GetMapping("/companies")
    @ResponseStatus(HttpStatus.OK)
    List<CompanyDto> getAllCompanies();

    //changed to response entity
    @GetMapping("/companies/{id}")
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity<CompanyDto> getCompany(@PathVariable Long id);

    //Added consumes and produces for testing
    @PostMapping("/companies")
    @ResponseStatus(HttpStatus.CREATED)
    CompanyDto addCompany(@RequestBody CompanyDto companyDto);

    @PutMapping("/companies/{id}")
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity<CompanyDto> updateCompany(@PathVariable Long id,@RequestBody CompanyDto companyDto);

    @DeleteMapping("/companies/{id}")
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity  deleteCompany(@PathVariable Long id);

    @DeleteMapping("/companies")
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity deleteAllCompanies();
}
