package com.managers;
;
import com.dtos.DepartmentDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RequestMapping("api/v1/companies")
public interface DepartmentControllerManger {
    @GetMapping("/{id}/departments")
    @ResponseStatus(HttpStatus.OK)
     List<DepartmentDto> getAllDepartments(@PathVariable Long id);

    @GetMapping("/{companyId}/departments/{id}")
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity<DepartmentDto> getDepartment(@PathVariable Long id, @PathVariable Long companyId);

    @PostMapping("/{id}/departments")
    @ResponseStatus(HttpStatus.CREATED)
    DepartmentDto addDepartment(@PathVariable Long id,@RequestBody DepartmentDto departmentDto);

    @PutMapping("/{companyId}/departments/{id}")
    @ResponseStatus(HttpStatus.OK)
    DepartmentDto updateDepartment(@RequestBody DepartmentDto departmentDto,@PathVariable Long id);

    @DeleteMapping("/{companyId}/departments/{id}")
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity deleteDepartment(@PathVariable Long id);

 /*   @DeleteMapping("/{companyId}/departments")
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity deleteAllDepartments();*/
}
