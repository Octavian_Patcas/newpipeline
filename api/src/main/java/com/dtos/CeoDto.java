package com.dtos;

public class CeoDto {
    private String name;
    private Long age;
    private CompanyDto companyDto;

    public CeoDto(){}
    public CeoDto(String name, Long age, CompanyDto companyDto) {
        this.name = name;
        this.age = age;
        this.companyDto = companyDto;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public CompanyDto getCompanyDto() {
        return companyDto;
    }

    public void setCompanyDto(CompanyDto companyDto) {
        this.companyDto = companyDto;
    }
}
