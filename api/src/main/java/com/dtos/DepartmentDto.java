package com.dtos;

public class DepartmentDto {

    private String name;
    private Long floor;
    private CompanyDto companyDto;

    public DepartmentDto(){}
    public DepartmentDto(String name, Long floor, CompanyDto companyDto) {
        this.name = name;
        this.floor = floor;
        this.companyDto= companyDto;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getFloor() {
        return floor;
    }

    public void setFloor(Long floor) {
        this.floor = floor;
    }

    public CompanyDto getCompanyDto() {
        return companyDto;
    }

    public void setCompanyDto(CompanyDto companyDto) {
        this.companyDto = companyDto;
    }

}
